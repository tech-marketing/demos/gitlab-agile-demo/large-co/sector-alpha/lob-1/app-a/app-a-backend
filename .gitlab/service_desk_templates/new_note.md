As we currently work on your submitted ticket, we'd like to let you know a new comment was made.

New Comment: %{NOTE_TEXT}

Please copy and paste this url to view the issue directly %{ISSUE_PATH}